﻿using System;

namespace lab_1
{
    class Program
    {
        static int fact(int n)
        {
            int res = 1;

            if (n == 0)
                return 1;

            for (int i = 1; i <= n; i++)
                res *= i;

            return res;
        }

        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            //n = 40585;

            int res = 0;

            for (int i = 1; i <= n; i++)
            {
                for (int temp = i; (temp / 10.0) >= 0.1; temp /= 10)
                {
                    res += fact(temp % 10);
                }
                if (res == i) {
                    string str = i.ToString();

                    for (int j = 0; j < str.Length - 1; j++)
                        Console.Write(str[j] + "! + ");
                    Console.WriteLine(str[str.Length - 1] + "! = " + str);
                }


                res = 0;

            }


            //foreach (char ch in n)
            //    res += fact(Int32.Parse(ch.ToString()));

            //Console.WriteLine(res);
        }
    }
}
